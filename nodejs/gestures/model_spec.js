/* global describe, it, sandbox, dbclient, beforeEach */
/* eslint-disable import/no-extraneous-dependencies, no-unused-expressions */

require('../shared/helper_spec.js');
const expect = require('chai').expect;
const Gesture = require('./model').Gesture;
const Action = require('../actions/model').Action;
const Event = require('../events/model').Event;

describe('Gesture', () => {
  describe('Model', () => {
    describe('#random', () => {
      it('should generate random integer', () => {
        const gesture = Gesture.random({});
        expect(gesture.integer).to.exist;
      });
      it('should generate id for the gesture', () => {
        const gesture = Gesture.random({});
        expect(gesture.id).to.exist;
      });
      it('should set device and user from those provided', () => {
        const gesture = Gesture.random({ user: 'f7b5c8bd-f182-4696-93b2-8b53e33db407', device: 'c4a8b5fe-a725-4f6b-9ab0-81af5bb1f863' });
        expect(gesture.user).to.eql('f7b5c8bd-f182-4696-93b2-8b53e33db407');
        expect(gesture.device).to.eql('c4a8b5fe-a725-4f6b-9ab0-81af5bb1f863');
      });
      it('should generate pattern from the integer', () => {
        const gesture = Gesture.random();
        expect(parseInt(gesture.pattern.replace(/\*/g, '1').replace(/_/g, '0'), 2)).to.eql(gesture.integer);
      });
    });
    describe('#execute', () => {
      beforeEach(() => {
        sandbox.stub(Action, 'findAllById', (ids, callback) => callback(null, { data: [
            { attributes: { id: 'a9d66b7b-ee58-4a66-a876-11c58136c843', type: 'lifx' } },
            { attributes: { id: 'a9d66b7b-ee58-4a66-a876-11c58136c841', type: 'email' } },
        ] }));
        sandbox.stub(Event, 'save', (e, cb) => cb(null));
        sandbox.stub(Action, 'execute', (action, callback) => callback(null, 'result'));
      });
      it('should return array of executed action types', (done) => {
        Gesture.execute({}, {}, (err, result) => {
          expect(result).to.eql(['lifx', 'email']);
          done();
        });
      });
      it('should return array of executed action types for muted device', (done) => {
        Gesture.execute({}, { mute: true }, (err, result) => {
          expect(result).to.eql(['lifx', 'email']);
          done();
        });
      });
      it('should return array of executed action types for muted gesture', (done) => {
        Gesture.execute({ mute: true }, {}, (err, result) => {
          expect(result).to.eql(['lifx', 'email']);
          done();
        });
      });
      it('should send updated template of push notification', (done) => {
        Gesture.execute({ alert: true, integer: 13 }, {}, () => {
          expect(Action.execute.getCall(2).args[0].details.message.body).to.eql('● ● — ●\nTriggered');
          done();
        });
      });
    });
  });
});
