const _ = require('lodash');
const async = require('async');
const Action = require('../actions/model').Action;
const Device = require('../devices/model').Device;
const Gesture = require('./model').Gesture;
const Event = require('../events/model').Event;

exports.create = (req, res, next) => {
  const attributes = req.body.data[0].attributes;
  attributes.id = Gesture.id();
  attributes.user = req.user.id;

  attributes.pattern = Gesture.pattern(attributes.integer);
  return Gesture.validate(attributes, (err) => {
    if (err) return next(err);

    return Device.findById(attributes.device, (err, device) => {
      if (err) return next(err);
      if (!device) {
        return next(Device.notFound());
      }
      if (device.user !== attributes.user) {
        return res.json({ errors: [{ status: '403', code: 'forbidden', title: 'Device owned by another user', source: { pointer: '/data/attributes/device' } }] });
      }

      return Action.findAllById(attributes.actions, (err, actionResult) => {
        if (err) return next(err);

        const actions = actionResult.data;
        if (attributes.actions && attributes.actions.length &&
          (!actions || !actions.length ||
          (attributes.actions && actions.length !== attributes.actions.length))) {
          return next(Action.notFound());
        }

        return Gesture.save(attributes, (err, gesture) => {
          if (err) return next(err);

          // Update actions
          return async.each(actions, (action, done) => {
            if (action.gesture && action.gesture !== gesture.id) {
              return done({ errors: [{ status: '400', code: 'duplication', title: 'Action assigned to another gesture', source: { pointer: '/data/attributes/actions' } }] });
            }

            action.gesture = gesture.id;
            return Action.save(action, done);
          }, (err) => {
            if (err && err.errors) return res.json(err);
            if (err) return next(err);

            device.gestures = (device.gestures || []).concat(attributes.id);
            return Device.save(device, (err) => {
              if (err) return next(err);

              return Event.create({
                id: Event.id(),
                user: device.user,
                device: device.id,
                timestamp: new Date().getTime(),
                type: 'gesture.created',
                details: {
                  gesture,
                },
              }, (err) => {
                if (err) return next(err);
                return res.json(Gesture.toJson(gesture));
              });
            });
          });
        });
      });
    });
  });
};

exports.index = (req, res, next) => {
  return Device.findById(req.params.device, (err, device) => {
    if (err) return next(err);

    return Gesture.findAllById(device.gestures, (err, gestures) => {
      if (err) return next(err);

      return res.json(gestures);
    });
  });
};

exports.delete = (req, res, next) => {
  return Gesture.findById(req.params.id, (err, gesture) => {
    if (err) return next(err);

    return async.parallel([
      (done) => {
        return Device.findById(gesture.device, (err, device) => {
          if (err) return done(err);

          device.gestures = device.gestures.filter(id => id !== gesture.id);
          return Device.save(device, done);
        });
      }, (done) => {
        return Action.findAllById(gesture.actions, (err, actions) => {
          if (err) return done(err);
          return async.each(actions.data.map(a => a.attributes), (action, callback) => {
            delete action.gesture;
            return Action.save(action, callback);
          }, done);
        });
      },
    ], (err) => {
      if (err) return next(err);

      return Gesture.removeById(gesture.id, (err) => {
        if (err) return next(err);
        return res.json(Gesture.toJson(gesture));
      });
    });
  });
};

exports.update = (req, res, next) => {
  const attributes = _.get(req, 'body.data[0].attributes');
  Gesture.findById(req.params.id, (err, gesture) => {
    if (err) return next(err);
    if (gesture.user !== req.user.id) {
      return next(Gesture.notFound());
    }
    const errors = ['user', 'device', 'actions'].map((field) => {
      if (attributes[field] && attributes[field] !== gesture[field]) {
        return { status: '400', code: 'any.not_allowed', title: `${_.capitalize(field)} change is not allowed`, source: { pointer: `/data/attributes/${field}` } };
      }
      return null;
    }).filter(e => !!e);

    if (errors && errors.length) return next({ errors });

    return Gesture.validate(Object.assign({},
      gesture,
      attributes,
      { pattern: attributes.integer !== undefined
          ? Gesture.pattern(attributes.integer)
          : gesture.pattern,
      }), (err, newGesture) => {
      if (err) return next(err);
      return Gesture.save(newGesture, (err, updatedGesture) => {
        if (err) return next(err);

        return async.map([
          {
            id: Event.id(),
            user: gesture.user,
            device: gesture.device,
            timestamp: new Date().getTime(),
            type: 'gesture.updated',
            details: {
              originGesture: gesture,
              gesture: newGesture,
            },
          },
        ], Event.create, (err) => {
          if (err) return next(err);

          return res.json(Gesture.toJson(updatedGesture));
        });
      });
    });
  });
};
