/* dom.js */
const jsdom = require('jsdom').jsdom;

const exposedProperties = ['window', 'navigator', 'document'];

global.document = jsdom('');
global.window = global.document.defaultView;
Object.keys(global.document.defaultView).forEach((property) => {
  if (typeof global[property] === 'undefined') {
    exposedProperties.push(property);
    global[property] = global.document.defaultView[property];
  }
});

global.navigator = {
  userAgent: 'node.js',
};

const storage = {
  USER: '{"role":"full_admin"}',
};

global.localStorage = {
  setItem: (key, value) => { storage[key] = value; },
  getItem: item => storage[item],
};
