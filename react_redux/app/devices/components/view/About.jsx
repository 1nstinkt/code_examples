import React from 'react';
import { Panel, Grid, Row, Col } from 'react-bootstrap';
import { Link } from 'react-router';
import moment from 'moment';
import _ from 'lodash';

import  './Styles.scss';

const About = (props) => {
  const device = props.device || {};
  const accountEvents = props.accountEvents;
  const firstLink = _.sortBy(_.filter(accountEvents.map(e => e.attributes), e => e.type === 'device.linked'), ['createdAt'])[0] || {};

  return (
    <Panel header={(<div><b>About:</b> {device.id}</div>)} className="about" id="about">
      <Grid fluid>
        <Row>
          <Col lg={5}>
            <label htmlFor="createdAt">MFG:</label>
          </Col>
          <Col id="createdAt">{moment(device.createdAt || 0).format('YYYY-MM-DD [at] H:mm:ss')}</Col>
        </Row>
        <Row>
          <Col lg={5}>
            <label htmlFor="firmware">Installed FW:</label>
          </Col>
          <Col id="firmware"><Link to={`/firmware/${_.get(device, 'status.reported.firmware')}`}>{_.get(device, 'status.reported.firmware')}</Link></Col>
        </Row>
        <Row>
          <Col lg={5}>
            <label htmlFor="firmware">Deployed FW:</label>
          </Col>
          <Col id="firmware"><Link to={`/firmware/${_.get(device, 'status.desired.firmware')}`}>{_.get(device, 'status.desired.firmware')}</Link></Col>
        </Row>
        <Row>
          <Col lg={5}>
            <label htmlFor="warranty">Warranty:</label>
          </Col>
          <Col id="warranty">
            { firstLink.createdAt ? `valid through ${moment(firstLink.createdAt || 0).add(365, 'days').format('YYYY-MM-DD')}` : 'not initiated' }
          </Col>
        </Row>
        <Row>
          <Col lg={5}>
            <label htmlFor="lifecycle">Lifecycle Stage:</label>
          </Col>
          <Col id="lifecycle">{(device.user && 'user activated') || (device.used && 'used, unlined') || 'new'}</Col>
        </Row>
        <Row>
          <Col lg={5}>
            <label htmlFor="location">Location:</label>
          </Col>
          <Col id="location">{['location.name', 'location.ciy', 'location.state', 'location.country'].map(l => _.get(device, l)).join(' ')}</Col>
        </Row>
      </Grid>
    </Panel>
  );
};

About.propTypes = {

};

export default About;
