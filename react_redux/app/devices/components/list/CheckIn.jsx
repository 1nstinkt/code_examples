import _ from 'lodash';
import moment from 'moment';
import React from 'react';
import { Label } from 'react-bootstrap';

const CheckIn = (props) => {
  const activated = _.get(props, 'activated');
  let minutes = null;
  let hours = null;
  let days = null;

  if (props.lastCheckIn) {
    const latestCheckIn = moment(props.lastCheckIn);
    minutes = moment(new Date()).diff(latestCheckIn, 'minutes');
    hours = moment(new Date()).diff(latestCheckIn, 'hours');
    days = moment(new Date()).diff(latestCheckIn, 'days');
  }

  let buttonStyle = '';
  let buttonText = '';

  if (!activated || props.lastCheckIn === undefined) {
    buttonStyle = 'default';
    buttonText = 'never';
  } else if (minutes <= 60) {
    buttonStyle = 'success';
    buttonText = `${minutes} minute${minutes > 1 ? 's' : ''} ago`;
  } else if (minutes > 60 && hours <= 24) {
    buttonStyle = 'warning';
    buttonText = `${hours} hour${hours > 1 ? 's' : ''} ago`;
  } else {
    buttonStyle = 'danger';
    buttonText = `${days} day${days > 1 ? 's' : ''} ago`;
  }

  return (
    <Label bsStyle={buttonStyle}>{buttonText}</Label>
  );
};

export default CheckIn;

