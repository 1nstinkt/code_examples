import { expect, it, describe, beforeEach } from 'chai';
import sinon from 'sinon';
import Enzyme, { mount, render, shallow } from 'enzyme';
// setup adapter
import Adapter from 'enzyme-adapter-react-15.4';

Enzyme.configure({ adapter: new Adapter() });

// define global variables for further tests
global.expect = expect;
global.it = it;
global.describe = describe;
global.sinon = sinon;
global.beforeEach = beforeEach;

global.mount = mount;
global.render = render;
global.shallow = shallow;
