import _ from 'lodash';
import React from 'react';
import { Label } from 'react-bootstrap';

const Signal = (props) => {
  const activated = _.get(props, 'activated');
  const signal = _.get(props, 'status.reported.signal');

  let buttonStyle = '';
  let buttonText = '';

  if (!activated || signal === undefined) {
    buttonStyle = 'default';
    buttonText = 'n/a';
  } else if (signal > -60) {
    buttonStyle = 'success';
    buttonText = 'normal';
  } else if (signal <= -60) {
    buttonStyle = 'danger';
    buttonText = 'weak';
  }

  return (
    <Label bsStyle={buttonStyle}>{buttonText}</Label>
  );
};

export default Signal;

