import React from 'react';
import _ from 'lodash';
import { Table, Button } from 'react-bootstrap';
import { Link } from 'react-router';
import Spinner from '../../../shared/components/Spinner/index';
import ListNote from '../../../shared/components/ListNote';
import sessions from '../../../sessions/';

import Signal from './Signal';
import Battery from './Battery';
import CheckIn from './CheckIn';

const Grid = (props) => {
  const renderHeaders = () => {
    let headers = [
      {
        key: '_id',
        label: 'Device',
      },
      {
        key: 'location.country,location.state,location.city',
        label: 'Location',
      },
      {
        key: 'activated,status.reported.timestamp',
        label: 'Check-in',
      },
      {
        key: 'status.reported.signal',
        label: 'Signal',
      },
      {
        key: 'linkedEmail',
        label: 'Linked',
      },
      {
        key: 'status.reported.battery',
        label: 'Battery',
      },
      {
        key: 'status.reported.firmware',
        label: 'Firmware',
      },
      {
        key: 'issuesCount',
        label: 'Notes',
      },
    ];

    if (!props.userViewPage) {
      headers.splice(1, 0, {
        key: 'createdAt',
        label: 'Born On',
      });
    }

    if (!props.hideUnlinkBtn) {
      headers = headers.concat([{
        key: 'user',
        label: 'Unlink',
      }]);
    }

    headers = headers.map((item) => {
      const isSorting = typeof item.key !== 'undefined' && typeof (props.actions || {}).devicesSetSortBy !== 'undefined';
      const isSortingActive = _.get(props, 'sort.field') === item.key;
      const isSortingDirectionAsc = _.get(props, 'sort.direction') === 'asc';

      let sortingClass = isSorting ? 'sorting' : '';

      if (isSortingActive) {
        sortingClass = isSortingDirectionAsc ? 'sorting_asc' : 'sorting_desc';
      }

      /* eslint-disable jsx-a11y/no-static-element-interactions */
      return (
        <th
          key={item.label}
          className={`${item.className || ''} ${sortingClass}`}
          onClick={() => { item.key && props.actions && props.actions.devicesSetSortBy(item.key); }}
          role="list"
        >{item.label}</th>
      );
      /* eslint-enable jsx-a11y/no-static-element-interactions */
    });

    if (props.onDeviceChecked && props.onAllSelected) {
      headers = [(<th key="select" role="list">
        <input
          name="select.all"
          type="checkbox"
          checked={!_.differenceBy(props.devices, props.selected, d => d.id).length}
          onChange={(event) => {
            props.onAllSelected(event.target.checked);
            props.onDeviceChecked(props.devices, event.target.checked);
          }}
        />

      </th>)].concat(headers);
    }

    return headers;
  };

  const renderCheckboxes = (device) => {
    if (!props.onDeviceChecked && !props.onAllSelected) {
      return null;
    }
    return (<td>
      <input
        name="select"
        type="checkbox"
        checked={!!(props.selected || []).find(s => s.id === device.id)}
        onChange={(event) => {
          props.onDeviceChecked(device, event.target.checked);
          if (event.target.checked
            && _.differenceBy(props.devices, props.selected, d => d.id).length === 1
          ) {
            props.onAllSelected(true);
          }
        }}
      />
    </td>);
  };

  const renderUnlinkBtns = (device) => {
    if (props.hideUnlinkBtn) return null;
    return (<td>
      {sessions.user().role === 'full_admin' && device.user ? (
        <Button bsStyle="danger" onClick={() => { props.deleteDevice(device.id); }}>
          UNLINK
        </Button>
      ) : null}
    </td>);
  };

  const devices = props.devices.filter((d) => {
    return _.get(props, 'view.devices.filter.deviceId') ? new RegExp(`^${_.get(props, 'view.devices.filter.deviceId')}`).test(d.id) : true;
  });

  if (props.inProgress) {
    return <Spinner />;
  }

  return (
    <Table className="table-framed datatable-basic dataTable no-footer devices">
      <thead>
        <tr>
          {renderHeaders(devices)}
        </tr>
      </thead>
      <tbody>{ devices && devices.map((device) => {
        const status = _.get(device, 'status.reported', {});

        return (<tr key={device.id}>
          {renderCheckboxes(device)}
          <td><Link to={`/devices/${device.id}`}>{device.id}</Link></td>
          {!props.userViewPage ? <td>{ (device.createdAt && new Date(device.createdAt).toISOString().replace(/T/, ' ').replace(/\.\d{3}Z$/, '')) || 'unknown'}</td> : null}
          <td>{Object.values(_.pick(_.get(device, 'location'), ['country', 'state', 'city'])).join(', ')}</td>
          <td>
            <CheckIn
              lastCheckIn={device.lastCheckIn}
              activated={device.activated}
            />
          </td>
          <td>
            <Signal
              status={device.status}
              activated={device.activated}
            />
          </td>
          <td>
            { device.linkedEmail ? (
              <Link to={`/users/${device.user}`}>{device.linkedEmail}</Link>
            ) : 'No' }
          </td>
          <td>
            <Battery
              status={device.status}
              activated={device.activated}
            />
          </td>
          <td><Link to={`/firmware/${status.firmware}`}>{status.firmware}</Link></td>
          <td>{device.issues && Array.isArray(device.issues) && device.issues.length ?
            <ListNote
              {...props}
              issues={device.issues}
              entityId={device.id}
              getIssues={(issues, deviceId) => {
                return props.actions.getDeviceIssues(device.issues, deviceId);
              }}
            /> :
            <span className="issues-no_issues">0</span>}</td>
          {renderUnlinkBtns(device)}
        </tr>);
      })}
        {
        !props.inProgress && (!devices || !devices.length) && (
          <tr>
            <td>No Devices Found</td>
          </tr>
        )
      }
      </tbody>
    </Table>
  );
};

Grid.propTypes = {
  inProgress: React.PropTypes.bool,
  devices: React.PropTypes.arrayOf(React.PropTypes.shape({
    id: React.PropTypes.string,
    createdAt: React.PropTypes.number,
    status: React.PropTypes.shape({}),
  })),
};

Grid.defaultProps = {
  inProgress: false,
  devices: [],
  firmwares: null,
};

export default Grid;
