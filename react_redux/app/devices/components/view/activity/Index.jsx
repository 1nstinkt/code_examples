import _ from 'lodash';
import React from 'react';
import { Nav, NavItem, Tab } from 'react-bootstrap';

import Hardware from './Hardware';
import Usage from './Usage';
import Pagination from '../../../../shared/components/Pagination';
import PageSize from '../../../../shared/components/PageSize';

const Activity = (props) => {
  let usageTab = null;
  const events = {
    hardware: _.get(props, 'view.events.hardware.data', []),
    usage: _.get(props, 'view.events.usage.data', []),
  };
  return (
    <div className="panel panel-default">
      <div className="panel-heading">
        <h4>Device Activity</h4>
      </div>
      <div className="panel-body">
        <div style={{ display: 'inline-block', height: '50px' }} />
        <PageSize
          {...props.view.events}
          actions={props.actions}
          type={props.type}
        />
        <Tab.Container id="events" defaultActiveKey="hardware">
          <div>
            <Nav justified bsStyle="tabs" className="nav-tabs-sm nav-tabs-solid">
              <NavItem
                eventKey="hardware"
                onSelect={() => {
                  props.applyFilter({
                    tags: 'hardware.changed',
                    type: {
                      $in: [],
                    },
                  }, 'hardware');
                }}
              >
                Hardware State Changes
              </NavItem>
              <NavItem
                eventKey="usage"
                onSelect={() => {
                  props.applyFilter({
                    tags: {
                      $in: [],
                    },
                    type: {
                      $in: ['gesture.updated', 'device.update.location', 'device.linked'],
                    },
                  }, 'usage');
                }}
              >
                Usage History
              </NavItem>
            </Nav>
            <Tab.Content>
              <Tab.Pane eventKey="hardware">
                <Hardware
                  events={events.hardware}
                  inProgress={props.inProgress}
                />
              </Tab.Pane>
              <Tab.Pane
                eventKey="usage"
                onEnter={() => { usageTab.clearFilters(); }}
              >
                <Usage
                  ref={(i) => { usageTab = i; }}
                  type="devices"
                  events={events.usage}
                  inProgress={props.inProgress}
                  applyFilter={(filters) => { props.applyFilter(filters, 'usage'); }}
                />
              </Tab.Pane>
            </Tab.Content>
          </div>
        </Tab.Container>
      </div>
      <Pagination
        {...props.view.events}
        actions={props.actions}
        type={props.type}
      />
    </div>
  );
};

Activity.propTypes = {

};

export default Activity;
