module.exports.setup = (app, controller, admin = null, middlewares = []) => {
  app.post('/gestures', middlewares, controller.create);
  app.delete('/gestures/:id', middlewares, controller.delete);
  app.put('/gestures/:id', middlewares, controller.update);
};
