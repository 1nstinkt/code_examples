/* eslint-disable import/no-extraneous-dependencies, no-unused-expressions */

'use strict';

require('../shared/helper_spec');
const Device = require('../devices/model').Device;
const Action = require('../actions/model').Action;
const Gesture = require('./model').Gesture;
const Event = require('../events/model').Event;

const expect = require('chai').expect;
const controller = require('./controller.js');

describe('Gesture', () => {
  describe('Controller', () => {
    describe('#create', () => {
      beforeEach(() => {
        req.body = {
          data: [{
            type: 'gestures',
            attributes: {
              integer: 27,
              actions: ['d8570283-01b5-4425-a3fb-40433bd8ae82', '80ea58c6-f3a8-4686-ae5e-abdb58ab1c34'],
              device: '80963157-7939-4502-836a-ea746fd3806c',
              user: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
            },
          }],
        };

        req.user = { id: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f', email: 'monkey@zoo.com' };
        sandbox.stub(Device, 'findById', (id, callback) => callback(null, {
          id: '80963157-7939-4502-836a-ea746fd3806c',
          gestures: ['gesture1', 'gesture2', 'gesture3'],
          user: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
        }));
        sandbox.stub(Device, 'save', (attributes, callback) => callback(null, attributes));

        sandbox.stub(Gesture, 'save', (attributes, callback) => callback(null, attributes));
        sandbox.stub(Gesture, 'findAllById', (ids, callback) => callback(null, { data: [{ gesture: 23 }, { gesture: 28 }, { gesture: 3 }] }));

        sandbox.stub(Action, 'findAllById', (ids, callback) => callback(null, {
          data: [
            { id: 'd8570283-01b5-4425-a3fb-40433bd8ae82' },
            { id: '80ea58c6-f3a8-4686-ae5e-abdb58ab1c34', gestures: ['c38f6851-1591-4c50-8d9c-6bf623bc951d'] },
          ],
        }));
        sandbox.stub(Action, 'save', (action, callback) => callback());
        sandbox.stub(Event, 'save', (event, callback) => callback());
        sandbox.stub(Event, 'id').returns('935e23ba-3e97-4849-99e7-be2308b5d828');
      });
      it('should reject if incoming data is invalid', (done) => {
        delete req.body.data[0].attributes.device;
        return controller.create(req, res, (err) => {
          expect(err).to.exist;
          expect(err.isJoi).to.be.true;
          expect(err.details[0].type).to.eql('any.required');
          expect(err.details[0].message).to.eql('"device" is required');
          expect(err.details[0].path).to.eql('device');
          done();
        });
      });
      it('should reject if device does not exist', (done) => {
        req.body.data[0].attributes.device = '80963157-7939-4502-836a-ea746fd3806c';
        Device.findById.restore();
        sandbox.stub(Device, 'findById', (id, callback) => callback('not found'));

        controller.create(req, res, (err) => {
          expect(err).to.eql('not found');
          done();
        });
      });
      it('should reject if actions provided do not exist', (done) => {
        Action.findAllById.restore();
        sandbox.stub(Action, 'findAllById', (ids, callback) => callback(null, { data: [] }));

        controller.create(req, res, (err) => {
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '404', code: 'presence', title: 'Action not found', source: { pointer: 'action.id' } });
          done();
        });
      });
      it('should skip actions verification if no actions provided', (done) => {
        delete req.body.data[0].attributes.actions;

        res.json = (r) => {
          expect(Gesture.save.called).to.be.true;
          expect(r.errors).to.not.exist;
          done();
        };

        controller.create(req, res, next);
      });
      it('should reject if some of the provided actions do not exist', (done) => {
        req.body.data[0].attributes.actions = ['a9d66b7b-ee58-4a66-a876-11c58136c843', '324611d1-e568-49cb-9dfd-5a0cf2f39e6a'];
        Action.findAllById.restore();
        sandbox.stub(Action, 'findAllById', (ids, callback) => callback(null, { data: [{ id: 'a9d66b7b-ee58-4a66-a876-11c58136c843' }] }));

        controller.create(req, res, (err) => {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '404', code: 'presence', title: 'Action not found', source: { pointer: 'action.id' } });
          done();
        });
      });
      it('should reject if some of the actions have another gesture assigned', (done) => {
        req.body.data[0].attributes.actions = ['a9d66b7b-ee58-4a66-a876-11c58136c843', '324611d1-e568-49cb-9dfd-5a0cf2f39e6a'];
        Action.findAllById.restore();
        sandbox.stub(Action, 'findAllById', (ids, callback) => callback(null, {
          data: [
            { id: 'a9d66b7b-ee58-4a66-a876-11c58136c843' },
            { id: '324611d1-e568-49cb-9dfd-5a0cf2f39e6a', gesture: 'c6b9a49c-c4c5-4e26-a463-1110859aa8af' },
          ],
        }));

        res.json = (r) => {
          expect(r.errors).to.exist;
          expect(r.errors[0]).to.eql({ status: '400', code: 'duplication', title: 'Action assigned to another gesture', source: { pointer: '/data/attributes/actions' } });
          done();
        };

        controller.create(req, res, next);
      });
      it('should create valid gesture', (done) => {
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].device).to.eql('80963157-7939-4502-836a-ea746fd3806c');
          done();
        };
        controller.create(req, res, next);
      });
      it('should ignore actions if no action id was provided', (done) => {
        delete req.body.data[0].attributes.actions;
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0]).to.exist;
          expect(Gesture.save.getCall(0).args[0].actions).to.not.exist;
          done();
        };

        controller.create(req, res, next);
      });
      it('should set current user as owner', (done) => {
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].user).to.eql('d2a85ac0-ffdc-4ebf-8023-58478b904d0f');
          done();
        };

        controller.create(req, res, next);
      });
      it('should generate uuid id', (done) => {
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].id).to.exist;
          done();
        };

        controller.create(req, res, next);
      });
      it('should add gesture to the device gesture list', (done) => {
        res.json = () => {
          expect(Device.save.called).to.be.true;
          expect(Device.save.getCall(0).args[0].gestures.length).to.eql(4);
          done();
        };

        controller.create(req, res, next);
      });
      it('should reject with 403 Forbidden if user does not own the device', (done) => {
        req.user = { id: '7a4a4397-6fa7-4a15-9505-990e8c93fe12', email: 'baboon@zoo.com' };
        res.json = (r) => {
          expect(r.errors).to.exist;
          expect(r.errors[0]).to.eql({ status: '403', code: 'forbidden', title: 'Device owned by another user', source: { pointer: '/data/attributes/device' } });
          done();
        };

        controller.create(req, res, next);
      });
      it('should update actions with new gesture reference', (done) => {
        res.json = (r) => {
          expect(Action.save.called).to.be.true;
          expect(Action.save.getCall(0).args[0].gesture).to.eql(r.data[0].attributes.id);
          expect(Action.save.getCall(1).args[0].gesture).to.eql(r.data[0].attributes.id);
          done();
        };

        controller.create(req, res, next);
      });
      it('should convert integer implementation into pattern', (done) => {
        req.user = { id: '7a4a4397-6fa7-4a15-9505-990e8c93fe12', email: 'baboon@zoo.com' };
        res.json = (r) => {
          expect(r.errors).to.exist;
          expect(r.errors[0]).to.eql({ status: '403', code: 'forbidden', title: 'Device owned by another user', source: { pointer: '/data/attributes/device' } });
          done();
        };

        controller.create(req, res, next);
      });
      it('should create a \'Gesture Created\' event', (done) => {
        res.json = () => {
          expect(Event.save.called).to.be.true;
          const result = Event.save.getCall(0).args[0];
          delete result.details.gesture.id;
          expect(result).to.eql({
            id: '935e23ba-3e97-4849-99e7-be2308b5d828',
            user: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
            device: '80963157-7939-4502-836a-ea746fd3806c',
            timestamp: new Date().getTime(),
            type: 'gesture.created',
            details: {
              gesture: {
                actions: [
                  'd8570283-01b5-4425-a3fb-40433bd8ae82',
                  '80ea58c6-f3a8-4686-ae5e-abdb58ab1c34',
                ],
                device: '80963157-7939-4502-836a-ea746fd3806c',
                integer: 27,
                pattern: '**_**',
                user: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
              },
            },
          });
          done();
        };

        controller.create(req, res, next);
      });
      it('should use default value for notify field if it is not defined', (done) => {
        Gesture.validate(Object.assign({}, {
          id: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
          pattern: '**_**',
        }, req.body.data[0].attributes), (err, validated) => {
          expect(err).to.not.exist;
          expect(validated.alert).to.be.false;
          done();
        });
      });
      it('should demand only boolean value for notify field', (done) => {
        Gesture.validate(Object.assign({}, {
          id: 'd2a85ac0-ffdc-4ebf-8023-58478b904d0f',
          pattern: '**_**',
          alert: 'a_string',
        }, req.body.data[0].attributes), (err) => {
          expect(err).to.exist;
          expect(err.isJoi).to.be.true;
          expect(err.message).to.eql('child "alert" fails because ["alert" must be a boolean]');
          done();
        });
      });
    });
    describe('#index', () => {
      var result = null;
      beforeEach((done) => {
        req.params.device = 'deviceid';
        sandbox.stub(Device, 'findById', (id, callback) => callback(null, { gestures: ['gesture1', 'gesture2'] }));
        sandbox.stub(Gesture, 'findAllById', (id, callback) => callback(null, { data: [{ type: 'gestures', attributes: { integer: 21 } }] }));
        res.json = (r) => {
          result = r;
          done();
        };
        controller.index(req, res, next);
      });
      it('should return all gestures per device', () => {
        expect(Device.findById.called).to.eql(true);
        expect(Device.findById.getCall(0).args[0]).to.eql('deviceid');
      });
      it('should request all gestures for the device', () => {
        expect(Gesture.findAllById.called).to.be.true;
        expect(Gesture.findAllById.getCall(0).args[0]).to.eql(['gesture1', 'gesture2']);
      });
      it('should respond with the gesture found', () => {
        expect(result).to.exist;
        expect(result).to.eql({ data: [{ type: 'gestures', attributes: { integer: 21 } }] });
      });
    });
    describe('#delete', () => {
      let gesture = null;
      beforeEach((done) => {
        gesture = {
          actions: ['24335738-0b73-4e3d-9533-bb997f730002', '5e5764ec-642c-4dba-9c1a-5b902fa33bdd'],
          device: '64bb01e0-64ef-4783-be40-73fc27dbe906',
          integer: 27,
          pattern: '***_*_',
          user: '17030eae-cfed-4926-88b4-d6b128f04c12',
          id: 'cbdefc0a-900e-4789-9757-649459a80cdc',
        };
        req.params.id = gesture.id;

        sandbox.stub(Gesture, 'findById', (id, callback) => callback(null, gesture));
        sandbox.stub(Device, 'findById', (id, callback) => callback(null, {
          actions: ['some1', 'some2'],
          gestures: ['gesture1', 'cbdefc0a-900e-4789-9757-649459a80cdc', 'gesture3'],
        }));
        sandbox.stub(Device, 'save', (id, callback) => callback());
        sandbox.stub(Action, 'findAllById', (id, callback) => callback(null, {
          data: [{
            type: 'actions',
            attributes: { id: 'action1', gesture: gesture.id },
          }, {
            type: 'actions',
            attributes: { id: 'action2', gesture: gesture.id },
          }],
        }));
        sandbox.stub(Action, 'save', (id, callback) => callback());
        sandbox.stub(Gesture, 'removeById', (id, callback) => callback());
        done();
      });
      it('should respond with not found if no gesture id provided', (done) => {
        Gesture.findById.restore();

        controller.delete(req, res, (err) => {
          expect(err).to.exist;
          expect(err).to.eql({ errors: [{
            status: '404',
            code: 'presence',
            title: 'Gesture not found',
            source: { pointer: 'gesture.id' },
          }] });
          done();
        });
      });
      it('should unreference the gesture from device', (done) => {
        res.json = () => {
          expect(Device.findById.called).to.be.true;
          expect(Device.findById.getCall(0).args[0]).to.eql('64bb01e0-64ef-4783-be40-73fc27dbe906');
          expect(Device.save.called).to.be.true;
          expect(Device.save.getCall(0).args[0]).to.eql({ actions: ['some1', 'some2'], gestures: ['gesture1', 'gesture3'] });
          done();
        };
        controller.delete(req, res, next);
      });
      it('should unreference the gesture from all actions', (done) => {
        res.json = () => {
          expect(Action.findAllById.called).to.be.true;
          expect(Action.findAllById.getCall(0).args[0]).to.eql(['24335738-0b73-4e3d-9533-bb997f730002', '5e5764ec-642c-4dba-9c1a-5b902fa33bdd']);
          expect(Action.save.called).to.be.true;
          expect(Action.save.callCount).to.eql(2);
          expect(Action.save.getCall(0).args[0]).to.eql({ id: 'action1' });
          done();
        };
        controller.delete(req, res, next);
      });
      it('should remove the gesture', (done) => {
        res.json = () => {
          expect(Gesture.removeById.called).to.be.true;
          expect(Gesture.removeById.getCall(0).args[0]).to.eql('cbdefc0a-900e-4789-9757-649459a80cdc');
          done();
        };
        controller.delete(req, res, next);
      });
      it('should respond with gesture body once gesture deleted', (done) => {
        res.json = (result) => {
          expect(result).to.exist;
          expect(result.data[0]).to.eql({
            type: 'gestures',
            attributes: {
              actions: [
                '24335738-0b73-4e3d-9533-bb997f730002',
                '5e5764ec-642c-4dba-9c1a-5b902fa33bdd',
              ],
              device: '64bb01e0-64ef-4783-be40-73fc27dbe906',
              id: 'cbdefc0a-900e-4789-9757-649459a80cdc',
              integer: 27,
              pattern: '***_*_',
              user: '17030eae-cfed-4926-88b4-d6b128f04c12',
            },
          });
          done();
        };
        controller.delete(req, res, next);
      });
    });
    describe('#update', () => {
      beforeEach((done) => {
        req.params.id = '6eb45403-8b8c-40d4-8739-147417f966d3';
        req.user = { id: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433' };
        req.body.data = [{
          type: 'gestures',
          attributes: {
            user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
            name: 'Tripple Hit',
            integer: 27,
          },
        }];

        sandbox.stub(Gesture, 'findById', (id, callback) => callback(null, {
          id: '6eb45403-8b8c-40d4-8739-147417f966d3',
          integer: 7,
          pattern: '***',
          user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
          device: 'KNC1-some',
          actions: ['67916899-164d-4311-a6ad-48d2beeda939', '65c9ff82-11fc-4383-9f3a-4b9f3fed5e14'],
        }));
        sandbox.stub(Gesture, 'save', (gesture, callback) => callback());
        sandbox.stub(Event, 'save', (event, callback) => callback());
        sandbox.stub(Event, 'id').returns('935e23ba-3e97-4849-99e7-be2308b5d828');
        done();
      });
      it('should block any change if gesture does not belong to logged in user', (done) => {
        req.user = { id: 'some' };
        controller.update(req, res, (err) => {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '404', code: 'presence', title: 'Gesture not found', source: { pointer: 'gesture.id' } });
          done();
        });
      });
      it('should block user change', (done) => {
        req.body.data[0].attributes.user = 'some';
        controller.update(req, res, (err) => {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '400', code: 'any.not_allowed', title: 'User change is not allowed', source: { pointer: '/data/attributes/user' } });
          done();
        });
      });
      it('should block device change', (done) => {
        req.body.data[0].attributes.device = 'device';
        controller.update(req, res, (err) => {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '400', code: 'any.not_allowed', title: 'Device change is not allowed', source: { pointer: '/data/attributes/device' } });
          done();
        });
      });
      it('should block actions change (for now)', (done) => {
        req.body.data[0].attributes.actions = ['action2'];
        controller.update(req, res, (err) => {
          expect(err).to.exist;
          expect(err.errors).to.exist;
          expect(err.errors[0]).to.eql({ status: '400', code: 'any.not_allowed', title: 'Actions change is not allowed', source: { pointer: '/data/attributes/actions' } });
          done();
        });
      });
      it('should change name if supplied', (done) => {
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].name).to.eql('Tripple Hit');
          done();
        };
        controller.update(req, res, next);
      });
      it('should change integer pattern if supplied', (done) => {
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].integer).to.eql(27);
          expect(Gesture.save.getCall(0).args[0].pattern).to.eql('**_**');
          done();
        };
        controller.update(req, res, next);
      });
      it('should respond with updated gesture', (done) => {
        Gesture.save.restore();
        sandbox.stub(Gesture, 'save', (gesture, callback) => callback(null, { forTest: true }));
        res.json = (result) => {
          expect(Gesture.save.called).to.be.true;
          expect(result).to.eql({ data: [{ type: 'gestures', attributes: { forTest: true } }] });
          done();
        };
        controller.update(req, res, next);
      });
      it('should collect gesture.updated event for user', (done) => {
        res.json = () => {
          expect(Event.save.called).to.be.true;
          const result = Event.save.getCall(0).args[0];
          expect(result).to.eql({
            id: '935e23ba-3e97-4849-99e7-be2308b5d828',
            user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
            device: 'KNC1-some',
            timestamp: new Date().getTime(),
            type: 'gesture.updated',
            details: {
              gesture: {
                actions: [
                  '67916899-164d-4311-a6ad-48d2beeda939',
                  '65c9ff82-11fc-4383-9f3a-4b9f3fed5e14',
                ],
                device: 'KNC1-some',
                id: '6eb45403-8b8c-40d4-8739-147417f966d3',
                integer: 27,
                mute: false,
                name: 'Tripple Hit',
                alert: false,
                pattern: '**_**',
                user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
              },
              originGesture: {
                actions: [
                  '67916899-164d-4311-a6ad-48d2beeda939',
                  '65c9ff82-11fc-4383-9f3a-4b9f3fed5e14',
                ],
                device: 'KNC1-some',
                id: '6eb45403-8b8c-40d4-8739-147417f966d3',
                integer: 7,
                pattern: '***',
                user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
              },
            },
          });
          done();
        };
        controller.update(req, res, next);
      });
      it('should collect device data in gesture.updated event', (done) => {
        res.json = () => {
          expect(Event.save.called).to.be.true;
          const result = Event.save.getCall(0).args[0];
          expect(result).to.eql({
            details: {
              gesture: {
                actions: [
                  '67916899-164d-4311-a6ad-48d2beeda939',
                  '65c9ff82-11fc-4383-9f3a-4b9f3fed5e14',
                ],
                device: 'KNC1-some',
                id: '6eb45403-8b8c-40d4-8739-147417f966d3',
                integer: 27,
                mute: false,
                name: 'Tripple Hit',
                alert: false,
                pattern: '**_**',
                user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
              },
              originGesture: {
                actions: [
                  '67916899-164d-4311-a6ad-48d2beeda939',
                  '65c9ff82-11fc-4383-9f3a-4b9f3fed5e14',
                ],
                device: 'KNC1-some',
                id: '6eb45403-8b8c-40d4-8739-147417f966d3',
                integer: 7,
                pattern: '***',
                user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
              },
            },
            device: 'KNC1-some',
            id: '935e23ba-3e97-4849-99e7-be2308b5d828',
            timestamp: new Date().getTime(),
            type: 'gesture.updated',
            user: 'f6fd38f7-10e4-4b4c-9d37-43ac291a4433',
          });
          done();
        };
        controller.update(req, res, next);
      });
      it('should not update pattern if integer in undefined', (done) => {
        delete req.body.data[0].attributes.integer;
        res.json = () => {
          expect(Gesture.save.called).to.be.true;
          expect(Gesture.save.getCall(0).args[0].pattern).to.eql('***');
          done();
        };
        controller.update(req, res, next);
      });
    });
  });
});
