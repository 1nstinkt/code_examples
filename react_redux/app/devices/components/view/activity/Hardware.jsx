import React from 'react';
import _ from 'lodash';
import moment from 'moment';
import { Link } from 'react-router';
import { Table } from 'react-bootstrap';

import Battery from '../../list/Battery';
import Signal from '../../list/Signal';

const Hardware = (props) => {
  return (
    <Table condensed className="hardware">
      <thead>
        <tr>
          <th>Date/Time</th>
          <th>Battery</th>
          <th>Signal Strength</th>
          <th>Firmware</th>
        </tr>
      </thead>
      <tbody>
        { !props.inProgress && !props.events.length && (
          <tr>
            <td colSpan="3">
              No Events So Far
            </td>
          </tr>
        )}
        { props.inProgress && !props.events.length && (
          <tr>
            <td colSpan="3">
              Loading. Please wait...
            </td>
          </tr>
        )}
        { props.events && props.events.length > 0 && props.events.map((event, index) => {
          return event.attributes.details.device
            ? <tr key={[event.attributes.id, event.attributes.timestamp, index].join(':')}>
              <td>
                {moment(_.get(event, 'attributes.createdAt')).format('YYYY-MM-DD HH:mm A')}
              </td>
              <td>
                <Battery
                  status={_.get(event, 'attributes.details.device.status')}
                  activated={_.get(event, 'attributes.details.device.activated')}
                />
              </td>
              <td>
                <Signal
                  status={_.get(event, 'attributes.details.device.status')}
                  activated={_.get(event, 'attributes.details.device.activated')}
                />
              </td>
              <td>
                { _.get(event, 'attributes.details.device.status.reported.firmware') && (
                  <Link to={`/firmware/${_.get(event, 'attributes.details.device.status.reported.firmware')}`}>
                    {_.get(event, 'attributes.details.device.status.reported.firmware')}
                  </Link>
                )}
              </td>
            </tr>
            : null;
        })}
      </tbody>
    </Table>
  );
};

export default Hardware;
