/* eslint-disable */
import React, { Component, PropTypes } from 'react';
import { Panel, Grid, Row, Col, Table } from 'react-bootstrap';

class SingleDeviceGrid extends Component {
  render() {
    const device = this.props.device;

    return (
      <div>
        { !this.props.inProgress && device && (
          <Grid style={{ width: 'inherit' }}>
            <Row className="show-grid">
              <Col xs={12} md={5}>
                <Panel header={(<h3>About: </h3>)}>
                  <Table responsive className="spec-table">
                    <tbody>
                      <tr>
                        <td>Device</td>
                      </tr>
                      <tr>
                        <td>Uploaded</td>
                      </tr>
                      <tr>
                        <td>Uploaded</td>
                      </tr>
                      <tr>
                        <td>Uploaded</td>
                      </tr>
                      <tr>
                        <td>Uploaded</td>
                      </tr>
                    </tbody>
                  </Table>
                </Panel>
              </Col>
            </Row>
          </Grid>
          )
        }
      </div>
    );
  }
}

SingleDeviceGrid.propTypes = {
  device: PropTypes.shape({
    createdAt: PropTypes.number,
    model: PropTypes.string,
    notes: PropTypes.string,
  }),
  inProgress: PropTypes.bool,
};

SingleDeviceGrid.defaultProps = {
  device: null,
  inProgress: false,
};

export default SingleDeviceGrid;
