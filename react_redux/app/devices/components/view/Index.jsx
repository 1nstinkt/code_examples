import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, { Component } from 'react';

import { Modal, Row, Col, Button, ButtonToolbar, FormGroup, Dropdown, MenuItem } from 'react-bootstrap';
import { Typeahead } from 'react-bootstrap-typeahead';

import FontAwesome from 'react-fontawesome';
import _ from 'lodash';
import About from './About';
import Account from './Account';
import Notes from '../../../shared/components/Notes';
import Tags from '../../../shared/components/Tags';
import Activity from './activity/Index';
import { selector, actions } from '../../';
import DeployTime from '../../../shared/components/DeployTime';
import sessions from '../../../sessions/';

import './Styles.scss';

let deployTime = '3AM';

const deployTimeUpdate = (val) => {
  deployTime = val;
};

export class DevicesView extends Component {
  constructor(props) {
    super();
    this.state = {
      firmwareVersion: '',
      firmwareId: '',
      isChecked: false,
      filters: {
        device: _.get(props, 'params.id', 0),
        tags: {
          $in: ['hardware.changed'],
        },
      },
      tab: 'hardware',
    };
  }

  componentDidMount() {
    this.props.actions.getFirmwares();
    this.props.actions.getPreDevice(this.props.params.id);
    this.props.actions.getDevice(this.props.params.id);
    this.props.actions.getEvents(Object.assign(
      {},
      this.props.view.events.filter,
      this.state.filters,
    ), this.state.tab);
    this.getAccountEvents();
    this.props.actions.getDeviceTagSuggestions();
  }

  componentWillReceiveProps() {
    if (this.props.device !== undefined) {
      this.setState({
        isChecked: _.get(this, 'props.device.status.desired.debug', false),
      });
    }
  }

  componentWillUpdate(nextProps) {
    // events paging
    if (JSON.stringify(this.props.view.events.filter)
      !== JSON.stringify(nextProps.view.events.filter)
    ) {
      this.props.actions.getEvents(_.merge({},
        this.props.view.events.filter,
        nextProps.view.events.filter,
        this.state.filters,
      ), this.state.tab);
    }
  }

  getAccountEvents() {
    return this.props.actions.getAccountEvents({
      device: this.props.params.id,
      type: {
        $in: ['device.linked', 'device.unlinked'],
      },
    });
  }

  toggleChange() {
    const newState = !this.state.isChecked;
    this.setState({
      isChecked: newState,
    });
    this.props.actions.debugStatus(this.props.params.id, newState);
  }

  applyFilter(filters, tab) {
    this.setState({ filters: Object.assign({}, this.state.filters, filters), tab }, () => {
      if (this.props.view.events.filter.skip !== 0) return this.props.actions.resetPage();
      return this.props.actions.getEvents(Object.assign(
        {},
        this.props.view.events.filter,
        this.state.filters,
      ), this.state.tab);
    });
  }

  render() {
    const firmwares = this.props.firmwares;
    return (
      <div>
        <div className="page-header">
          <div className="page-header-content">
            <div className="page-title"><h4>Device Overview</h4>
              { sessions.user().role === 'full_admin' ?
                <Dropdown bsStyle="success" className="pull-right" style={{ marginTop: '-2em' }} id="action">
                  <Dropdown.Toggle noCaret bsStyle="danger" className="btn-labeled all-caps">
                    <b><FontAwesome name="bars" /></b>
                    ACTION
                  </Dropdown.Toggle>
                  <Dropdown.Menu className="firmware-deploy-options">
                    <MenuItem eventKey="1" onSelect={() => this.props.actions.showDeployFirmware()}>DEPLOY TO DEVICE</MenuItem>
                    <MenuItem eventKey="2" onSelect={() => this.props.actions.provisionMode(this.props.view.device.item.id)}>ENTER PROVISION MODE</MenuItem>
                    <MenuItem eventKey="3" onSelect={() => this.toggleChange()}>TOGGLE DEBUG</MenuItem>
                  </Dropdown.Menu>
                </Dropdown>
              : null }
            </div>
          </div>
        </div>
        <div className="page-container">
          <div className="page-content">
            <div className="content-wrapper">
              <Row>
                <Col lg={3}>
                  <Row id="about-row">
                    <About device={_.get(this, 'props.view.device.item')} accountEvents={_.get(this, 'props.view.accountEvents.data', [])} />
                  </Row>
                  <Row id="notes-row">
                    <Notes
                      {...this.props}
                      issues={_.get(this.props, 'view.device.issues', [])}
                      addNote={_.get(this.props, 'view.addNote', [])}
                      actions={{
                        addNote: _.get(this.props, 'actions.addDeviceIssue', () => {}),
                        showAddNote: _.get(this.props, 'actions.showAddDeviceIssue', () => {}),
                        cancel: _.get(this.props, 'actions.cancel', () => {}),
                        deleteNote: _.get(this.props, 'actions.deleteDeviceIssue', () => {}),
                        showEditIssue: _.get(this.props, 'actions.showEditDeviceIssue', () => {}),
                        editIssue: _.get(this.props, 'actions.editDeviceIssue', () => {}),
                      }}
                    />
                  </Row>
                  <Row id="notes-row">
                    <Tags
                      {...this.props}
                      saveTags={_.get(this.props, 'view.saveTags', {})}
                      tags={_.get(this.props, 'view.device.item.tags', []).map((v, i) => ({ id: i, text: v }))}
                      suggestions={_.get(this.props, 'view.tagSuggestions', [])}
                      actions={{
                        saveTags: _.get(this.props, 'actions.saveDeviceTags', () => {}),
                        showAddTag: _.get(this.props, 'actions.showAddDeviceTags', () => {}),
                        cancel: _.get(this.props, 'actions.cancel', () => {}),
                      }}
                    />
                  </Row>
                </Col>
                <Col lg={9}>
                  <Row id="account-row">
                    <Account {...this.props} />
                  </Row>
                  <Row id="activity-row">
                    <Activity
                      applyFilter={(filters, tab) => { this.applyFilter(filters, tab); }}
                      type="deviceView"
                      {...this.props}
                    />
                  </Row>
                </Col>
              </Row>
            </div>
          </div>
        </div>
        <Modal id="deploy" show={(this.props.deploy || {}).show} onHide={_.get(this.props, 'actions.cancel')}>
          <Modal.Header closeButton>
            <Modal.Title>Deploy to {`${_.get(this.props, 'params.id')}`}</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <FormGroup>
              <Typeahead
                className="firmware-typeahead"
                onChange={(selected) => {
                  this.state.firmwareId = selected[0];
                }}
                disabled={_.get(this.props, 'deploy.inProgress')}
                placeholder="Select Firmware to deploy"
                options={(firmwares || []).map(firmware => firmware.id)}
              />
              <DeployTime deployTimeUpdate={deployTimeUpdate} {...this.props} />
            </FormGroup>
            <hr />
            <p id="roll-back-notice">Once a deployment is initiated, it cannot be stopped.</p>
          </Modal.Body>
          <Modal.Footer>
            <ButtonToolbar className="pull-right">
              <Button onClick={_.get(this.props, 'actions.cancel')} className="all-caps">Cancel</Button>
              <Button
                className="all-caps"
                bsStyle="danger"
                disabled={_.get(this, 'props.deploy.inProgress')}
                onClick={() => {
                  this.props.actions.deployFirmware(
                    this.state.firmwareId,
                    this.props.params.id,
                    deployTime,
                  );
                  deployTimeUpdate('3AM');
                }}
              >
                {
                  _.get(this, 'props.deploy.inProgress') ? <FontAwesome name="circle-o-notch" spin /> : null
                } DEPLOY FIRMWARE
              </Button>
            </ButtonToolbar>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

export default connect(selector, dispatch => ({
  actions: bindActionCreators(actions, dispatch),
}))(DevicesView);
