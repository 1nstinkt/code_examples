import React from 'react';
import { DevicesView } from '../../app/devices/components/view/Index';
import About from '../../app/devices/components/view/About';
import Notes from '../../app/shared/components/Notes/index';
import Tags from '../../app/shared/components/Tags/index';
import Account from '../../app/devices/components/view/Account';
import reducer from '../../app/devices/index';

describe('Devices', () => {
  describe('View', () => {
    let wrapper = null;
    sinon.stub(DevicesView.prototype, 'componentDidMount').callsFake(() => {});
    wrapper = shallow(<DevicesView />);
    beforeEach(() => {});
    it('calls componentDidMount', () => {
      expect(DevicesView.prototype.componentDidMount.calledOnce).to.equal(true);
    });
    it('should have an about section', () => {
      expect(wrapper.find(About)).to.have.length(1);
    });
    it('should have a notes section', () => {
      expect(wrapper.find(Notes)).to.have.length(1);
    });
    it('should have an account section', () => {
      expect(wrapper.find(Account)).to.have.length(1);
    });
    it('should have a tags section', () => {
      expect(wrapper.find(Tags)).to.have.length(1);
    });
  });
  describe('Reducer', () => {
    let defaultState = {
      deploy: {},
      limits: {
        available: [
          { id: 100, text: '100' },
          { id: 250, text: '250' },
          { id: 500, text: '500' },
          { id: 1000, text: '1000' },
          { id: 0, text: 'Show All' },
        ],
        default: 100,
      },
      filter: {
        skip: 0,
      },
      sort: {
        field: null,
        direction: null,
      },
      meta: {
        count: 0,
        total: 0,
      },
      view: {
        device: {
          item: {},
          issues: [],
        },
        accountEvents: [],
        events: {
          filter: {
            skip: 0,
            limit: 10,
          },
          meta: {
            count: 0,
            total: 0,
          },
          limits: {
            available: [
              { id: 10, text: '10' },
              { id: 100, text: '100' },
              { id: 250, text: '250' },
              { id: 500, text: '500' },
              { id: 1000, text: '1000' },
              { id: 0, text: 'Show All' },
            ],
          },
        },
        addNote: {},
        issues: [],
        saveTags: {},
        tagSuggestions: [],
      },
      list: {
        devices: [],
        issues: {},
      },
    };
    it('should return the initial state', () => {
      expect(reducer(undefined, {})).eql(defaultState);
    });
    it('should reset account events on any page entrance', (done) => {
      const result = reducer(defaultState, {
        type: 'GET_PRE_DEVICE',
        payload: {
          id: 'any',
        },
      });
      expect(result.view.accountEvents).eql([]);
      return done();
    });
    it('should be able to unlink device if device doesn\'t have related user', () => {
      defaultState = {
        ...defaultState,
        list: {
          ...defaultState.user,
          devices: [
            {
              id: 'KNC-A1W-00000048',
            },
          ],
        },
      };
      const result = reducer(defaultState, {
        type: 'DELETE_DEVICE_COMPLETED',
        payload: {
          data: [],
        },
      });
      expect(result).eql({
        ...defaultState,
        inProgress: false,
        list: {
          ...defaultState.list,
          devices: [
            {
              id: 'KNC-A1W-00000048',
            },
          ],
        },
      });
    });
    describe('location changed', () => {
      it('should ignore location changes for other pages', () => {
        expect(reducer(defaultState, {
          type: '@@router/LOCATION_CHANGE',
          payload: {
            pathname: '/users',
          },
        })).eql(defaultState);
      });
      it('should store changed filter params to filter in state', () => {
        const query = {
          limit: 250,
          skip: 100,
        };

        expect(reducer(defaultState, {
          type: '@@router/LOCATION_CHANGE',
          payload: {
            pathname: '/devices',
            query,
          },
        })).eql(Object.assign({}, defaultState, {
          filter: {
            limit: 250,
            skip: 100,
          },
        }));
      });
      it('should use default limit if it is not defined in request', () => {
        const query = {
          skip: 100,
        };

        expect(reducer(defaultState, {
          type: '@@router/LOCATION_CHANGE',
          payload: {
            pathname: '/devices',
            query,
          },
        }).filter.limit).eql(defaultState.limits.default);
      });
    });
    describe('issues', () => {
      let stateWithDeviceIssues = null;
      let dummyDeviceId = null;
      beforeEach(() => {
        stateWithDeviceIssues = {
          ...defaultState,
          view: {
            ...defaultState.view,
            device: {
              ...defaultState.view.device,
              issues: [{
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                device: 'ff40c959-8fc4-4b55-a299-75653356a7de',
                description: 'aaa',
              }, {
                id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                device: 'ff40c959-8fc4-4b55-a299-75653356a7de',
                description: 'bbb',
              }],
            },
          },
          list: {
            ...defaultState.list,
            issues: {
              ...defaultState.list.issues,
              'ff40c959-8fc4-4b55-a299-75653356a7de': [{
                attributes: {
                  id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                  device: 'ff40c959-8fc4-4b55-a299-75653356a7de',
                  description: 'aaa',
                },
              }, {
                attributes: {
                  id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                  device: 'ff40c959-8fc4-4b55-a299-75653356a7de',
                  description: 'bbb',
                },
              }],
            },
          },
        };
        dummyDeviceId = 'ff40c959-8fc4-4b55-a299-75653356a7de';
      });
      it('should delete issue both from list and view param of state while issue was removed on device view page', (done) => {
        const result = reducer(stateWithDeviceIssues, {
          type: 'DELETE_DEVICE_ISSUE_COMPLETED',
          payload: {
            data: [{
              attributes: {
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                device: dummyDeviceId,
              },
            }],
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            device: {
              ...defaultState.view.device,
              issues: [{
                id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                description: 'bbb',
                device: dummyDeviceId,
              }],
            },
          },
          list: {
            ...defaultState.list,
            issues: {
              ...defaultState.list.issues,
              [dummyDeviceId]: [{
                attributes: {
                  id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                  description: 'bbb',
                  device: dummyDeviceId,
                },
              }],
            },
          },
        });
        expect(result.view.device.issues.length).eql(1);
        expect(result.list.issues[dummyDeviceId].length).eql(1);
        return done();
      });
      it('should update issue both in list and view param of state while issue was updated on device view page', (done) => {
        const result = reducer(stateWithDeviceIssues, {
          type: 'EDIT_DEVICE_ISSUE_COMPLETED',
          payload: {
            body: {
              data: [{
                attributes: {
                  id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                  device: dummyDeviceId,
                  description: 'aaa 111',
                },
              }],
            },
            deviceId: dummyDeviceId,
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            device: {
              ...defaultState.view.device,
              issues: [{
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                device: dummyDeviceId,
                description: 'aaa 111',
              }, {
                id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                device: dummyDeviceId,
                description: 'bbb',
              }],
            },
            addNote: {
              errors: [],
              show: false,
              toUpdate: null,
              inProgress: false,
            },
          },
          list: {
            ...defaultState.list,
            issues: {
              ...defaultState.list.issues,
              [dummyDeviceId]: [{
                attributes: {
                  id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                  device: dummyDeviceId,
                  description: 'aaa 111',
                },
              }, {
                attributes: {
                  id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                  device: dummyDeviceId,
                  description: 'bbb',
                },
              }],
            },
          },
        });
        expect(result.view.device.issues.length).eql(2);
        expect(result.list.issues[dummyDeviceId].length).eql(2);
        return done();
      });
      it('should add issue both to list and view param of state while issue was added on device view page', (done) => {
        const result = reducer(stateWithDeviceIssues, {
          type: 'ADD_DEVICE_ISSUE_COMPLETED',
          payload: {
            data: [{
              attributes: {
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f21',
                device: dummyDeviceId,
                description: 'ccc',
              },
            }],
            deviceId: dummyDeviceId,
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            device: {
              ...defaultState.view.device,
              issues: [{
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f21',
                device: dummyDeviceId,
                description: 'ccc',
              }, {
                id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                device: dummyDeviceId,
                description: 'aaa',
              }, {
                id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                device: dummyDeviceId,
                description: 'bbb',
              }],
            },
            addNote: {
              show: false,
              inProgress: false,
            },
          },
          list: {
            ...defaultState.list,
            issues: {
              ...defaultState.list.issues,
              [dummyDeviceId]: [{
                attributes: {
                  id: '3a831257-3dce-4ddf-8f58-c2f12c136f21',
                  device: dummyDeviceId,
                  description: 'ccc',
                },
              }, {
                attributes: {
                  id: '3a831257-3dce-4ddf-8f58-c2f12c136f46',
                  device: dummyDeviceId,
                  description: 'aaa',
                },
              }, {
                attributes: {
                  id: '11831257-3dce-4ddf-8f58-c2f12c136f34',
                  device: dummyDeviceId,
                  description: 'bbb',
                },
              }],
            },
          },
        });
        expect(result.view.device.issues.length).eql(3);
        expect(result.list.issues[dummyDeviceId].length).eql(3);
        return done();
      });
      it('should clear issues while view page loads', () => {
        const result = reducer(stateWithDeviceIssues, {
          type: 'GET_DEVICE_IN_PROGRESS',
        });
        expect(stateWithDeviceIssues.view.device.issues.length).eql(2);
        expect(result.view.device.issues.length).eql(0);
      });
    });
    describe('tags', () => {
      let result = null;
      beforeEach(() => {
        defaultState = {
          ...defaultState,
          device: {
            item: {
              id: 'KNC-A1W-T2783688',
              tags: ['aaa'],
            },
          },
          view: {
            ...defaultState.view,
            tagSuggestions: ['aaa', 'kkk', 'lll'],
          },
        };
      });
      it('should be able to preserve tag suggestions', () => {
        result = reducer(defaultState, {
          type: 'GET_DEVICE_TAG_SUGGESTIONS_COMPLETED',
          payload: {
            data: [{
              type: 'tags',
              attributes: {
                value: 'aaa',
              },
            }],
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            tagSuggestions: ['aaa'],
          },
        });
      });
      it('should be able to preserve updated tags', () => {
        result = reducer(defaultState, {
          type: 'SAVE_DEVICE_TAGS_COMPLETED',
          payload: {
            data: [{
              type: 'devices',
              attributes: {
                id: 'KNC-A1W-T2783688',
                tags: ['aaa', 'kkk', 'lll'],
              },
            }],
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            saveTags: {
              ...defaultState.view.saveTags,
              errors: [],
              inProgress: false,
              show: false,
            },
            device: {
              ...defaultState.view.device,
              item: {
                id: 'KNC-A1W-T2783688',
                tags: ['aaa', 'kkk', 'lll'],
              },
            },
          },
        });
      });
      it('should be able to update suggestions with new tag', () => {
        result = reducer(defaultState, {
          type: 'SAVE_DEVICE_TAGS_COMPLETED',
          payload: {
            data: [{
              type: 'devices',
              attributes: {
                id: 'KNC-A1W-T2783688',
                tags: ['aaa', 'bbb'],
              },
            }],
          },
        });
        expect(result).eql({
          ...defaultState,
          view: {
            ...defaultState.view,
            saveTags: {
              ...defaultState.view.saveTags,
              errors: [],
              inProgress: false,
              show: false,
            },
            tagSuggestions: ['aaa', 'kkk', 'lll', 'bbb'],
            device: {
              ...defaultState.view.device,
              item: {
                id: 'KNC-A1W-T2783688',
                tags: ['aaa', 'bbb'],
              },
            },
          },
        });
      });
    });
  });
});
