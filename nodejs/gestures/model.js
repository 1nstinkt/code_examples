const _ = require('lodash');
const Joi = require('joi');
const async = require('async');
const Model = require('../shared/model').Model;
const Action = require('../actions/model').Action;
const Event = require('../events/model').Event;

const Gesture = Model.define('gestures', Joi.object().keys({
  id: Joi.string().uuid().required(),
  name: Joi.string(),
  integer: Joi.number().required(),
  pattern: Joi.string().required(), // **_**, Use for reading only, do not use for searching
  mirror: Joi.number(), // device-specific integer. Read only.
  actions: Joi.array().items(Joi.string().uuid()),
  device: Joi.string().required(),
  user: Joi.string().required(),
  mute: Joi.boolean().default(false),
  alert: Joi.boolean().default(false), // push notification while gesture happens
}), {
  storage: 'mongo',
  notify: true, // fields to watch for and notify all devices while changes happened
});

Gesture.derefAction = (gestureId, actionId, done) => {
  if (!gestureId) return done();

  return Gesture.findById(gestureId, (err, gesture) => {
    if (err) return done(err);
    if (!gesture) return done();

    gesture.actions = (gesture.actions || []).filter(id => id !== actionId);
    return Gesture.save(gesture, done);
  });
};

Gesture.pattern = (integer) => {
  return (integer >>> 0).toString(2).replace(/1/g, '*').replace(/0/g, '_');
};

Gesture.fromPattern = (pattern) => {
  return parseInt((pattern || '').replace(/\*/g, '1').replace(/_/g, '0'), 2);
};

Gesture.random = (gesture) => {
  const integer = _.sample([27, 13, 7, 15, 31, 29, 59, 109, 219, 55, 111]);
  return Object.assign({
    id: Gesture.id(),
    integer,
    pattern: Gesture.pattern(integer),
  }, gesture);
};

Gesture.execute = (gesture, device, done) => {
  return Action.findAllById(gesture.actions, (err, actions) => {
    if (err) return done(err.stack || err);

    const gevent = {
      id: Event.id(),
      timestamp: new Date().getTime(),
      user: gesture.user,
      device: gesture.device,
      type: 'gesture',
      tags: [],
      details: {
        gesture,
        actions: actions.data.map(a => a.attributes),
        device,
      },
    };

    if (device.mute) gevent.tags = ['muted'];
    if (actions.data.some(a => a.attributes.type === 'fibaro') ? 'fibaro' : null) {
      gevent.tags = gevent.tags.concat('fibaro');
    }

    return Event.save(gevent, (err) => {
      if (err) return done(err);

      if (device.mute || gesture.mute) return done(null, actions.data.map(a => a.attributes.type));

      if (gesture.alert) {
        actions.data.push({
          attributes: {
            type: 'push',
            user: device.user,
            details: {
              message: {
                title: _.get(gesture, 'name', ''),
                body: (() => {
                  const firstLine = `${Gesture.pattern(gesture.integer).replace(/\*/g, '● ').replace(/_/g, '\u2014 ').slice(0, -1)}`;
                  const location = _.get(device, 'location.type') || _.get(device, 'location.name') ? `${_.get(device, 'location.type', '')} ${_.get(device, 'location.name', '')}` : false;
                  const secondLine = `Triggered${device.surface ? ` from ${device.surface}` : ''}${device.room ? ` in ${device.room}` : ''}${location ? ` at ${location}` : ''}`;
                  return `${firstLine}\n${secondLine}`;
                })(),
              },
            },
          },
        });
      }
      return async.map(actions.data.map(a => a.attributes), Action.execute, (err) => {
        if (err) return done(err);
        return done(null, actions.data.map(a => a.attributes.type));
      });
    });
  });
};

exports.Gesture = Gesture;
