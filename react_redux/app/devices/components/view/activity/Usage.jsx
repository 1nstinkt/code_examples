import React, { Component } from 'react';
import _ from 'lodash';
import { Table, Button, Checkbox, Form, FormGroup } from 'react-bootstrap';
import FontAwesome from 'react-fontawesome';
import moment from 'moment';

class Usage extends Component {
  constructor() {
    super();
    this.state = { filters: [] };
  }
  clearFilters() {
    this.setState({ filters: [] });
  }
  render() {
    // types for filtering
    const filterTypes = {
      'gesture.updated': 'Gesture',
      'device.update.location': 'Mounting Update',
      'device.linked': 'Wifi Provisioning (via app)',
    };

    const changeFilter = (e) => {
      const filters = this.state.filters;
      if (e.target.checked) {
        filters.push(e.target.id);
      } else {
        filters.splice(filters.indexOf(e.target.id), 1);
      }
      this.setState({ filters });
    };
    // applying this.state.filters for event types
    const applyFilter = (props) => {
      return props.applyFilter({ type: {
        $in: this.state.filters.length ? this.state.filters : ['gesture.updated', 'device.update.location', 'device.linked'],
      } });
    };

    const generateNotes = (type, details) => {
      let note = '';
      switch (type) {
        case 'gesture.updated':
          note = `Pattern: ${_.get(details, 'gesture.pattern', '').split('_').map(period => period.split('*').length).join(' + ')} taps >>>> ${_.get(details, 'gesture.actions', []).length} tasks triggered`;
          break;
        case 'device.update.location':
          note = `To: ${((details.device.location || {}).name || '')} ${((details.device.location || {}).type || '')} ${details.device.room ? `/ ${details.device.room}` : ''} ${details.device.orientation || details.device.surface ? '/' : ''}${details.device.orientation || ''} ${details.device.surface || ''} `;
          // 'location', 'room', 'orientation', 'surface'
          break;
        case 'device.linked':
          note = `at ${((details.device.location || {}).name || '')} ${((details.device.location || {}).type || '')} ${details.device.room ? `/ ${details.device.room}` : ''} ${details.device.orientation || details.device.surface ? '/' : ''}${details.device.orientation || ''} ${details.device.surface || ''} `;
          break;
        default:
          note = '';
          break;
      }

      return note;
    };

    return (
      <div className="usage">
        <div className="col-xs-12 filters">
          <label className="display-block text-semibold" htmlFor="filter">Filters</label>
          <Form inline>
            <FormGroup>
              {Object.keys(filterTypes).map((key) => {
                return (
                  <Checkbox
                    id={key}
                    key={key}
                    inline
                    checked={this.state.filters.indexOf(key) !== -1}
                    disabled={this.props.inProgress}
                    onChange={changeFilter}
                  >
                    {filterTypes[key]}
                  </Checkbox>
                );
              })}
            </FormGroup>
            <Button
              bsStyle="primary"
              disabled={this.props.inProgress}
              onClick={() => applyFilter(this.props)}
            >
              {
                this.props.inProgress ? <FontAwesome name="circle-o-notch" spin /> : null
              } Update
            </Button>
          </Form>
        </div>
        <Table className="usage-history" id="usage-history-table">
          <thead className="bg-knocki-white">
            <tr>
              <th>Date/Time</th>
              <th>Type</th>
              <th>Notes</th>
            </tr>
          </thead>
          <tbody className="text-center">
            { !this.props.inProgress && !this.props.events.length && (
              <tr>
                <td colSpan="3">
                  No Events So Far
                </td>
              </tr>
            )}
            { this.props.inProgress && !this.props.events.length && (
              <tr>
                <td colSpan="3">
                  Loading. Please wait...
                </td>
              </tr>
            )}
            { this.props.events
            && this.props.events.length > 0
            && this.props.events.map((event, index) => (
              <tr key={[event.attributes.id, event.attributes.timestamp, index].join(':')}>
                <td>
                  {moment(_.get(event, 'attributes.createdAt')).format('YYYY-MM-DD HH:mm:ss')}
                </td>
                <td>
                  {filterTypes[event.attributes.type] || ''}
                </td>
                <td>{generateNotes(event.attributes.type, event.attributes.details)}</td>
              </tr>
            )) }
          </tbody>
        </Table>
      </div>

    );
  }
}

export default Usage;

