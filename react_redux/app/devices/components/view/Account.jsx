import React from 'react';
import { Table } from 'react-bootstrap';
import moment from 'moment';
import  './Styles.scss';

const Account = (props) => {
  const events = ((props.view.accountEvents || []).data || []).map(event => event.attributes);
  return (
    <div className="panel panel-flat">
      <div className="panel-heading">
        <h4>Linked Account</h4>
      </div>
      <div className="panel-body">
        <Table responsive className="linked-account-table table-framed" id="linked-account-table">
          <thead className="bg-knocki-white">
            <tr>
              <th className="linked-account account" id="linked-account">Account:</th>
              <th className="linked-account status" id="linked-account">Status</th>
              <th className="linked-account date-linked" id="linked-account">Date Linked</th>
              <th className="linked-account action" id="linked-account">Action</th>
            </tr>
          </thead>
          <tbody>
            {events.map((event, index) => (
              <tr key={[event.id, event.timestamp, index].join(':')}>
                <td>{event.details.user ? <a href={`/users/${event.details.user.id}`}>{event.details.user.email}</a> : 'n/a'}</td>
                <td>{event.type === 'device.linked' ? <span className="label label-success">LINKED</span> : `unlinked (${moment(event.createdAt).format('YYYY-MM-DD HH:mm:ss')})`}</td>
                {events[index + 1] ? <td>{event.type === 'device.linked' ? moment(event.createdAt).format('YYYY-MM-DD HH:mm:ss') : moment(events[index + 1].createdAt).format('YYYY-MM-DD HH:mm:ss')}</td> : <td>{event.type === 'device.linked' ? moment(event.createdAt).format('YYYY-MM-DD HH:mm:ss') : 'n/a'}</td>}
                <td>{event.type === 'device.unlinked' ? 'n/a' : <button className="unlink-device" onClick={() => props.actions.deleteDevice(event.device)}>UNLINK</button>}</td>
              </tr>
            ))}
          </tbody>
        </Table>
      </div>
    </div>
  );
};

Account.propTypes = {
};

export default Account;
