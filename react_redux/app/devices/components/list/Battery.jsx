import _ from 'lodash';
import React from 'react';
import { Label } from 'react-bootstrap';

const Battery = (props) => {
  const battery = _.get(props, 'status.reported.battery');
  const activated = _.get(props, 'activated');

  let buttonStyle = '';
  if (!activated || battery === undefined) {
    buttonStyle = 'default';
  } else if (battery > 252) {
    buttonStyle = 'success';
  } else if (battery <= 252 && battery > 246) {
    buttonStyle = 'warning';
  } else if (battery <= 246) {
    buttonStyle = 'danger';
  }

  return (
    <Label bsStyle={buttonStyle}>{battery ? `${battery / 100}V` : 'n/a'}</Label>
  );
};

export default Battery;
