import _ from 'lodash';
import { createActions } from 'redux-actions';
import queryString from 'qs';
import moment from 'moment';

const initialState = {
  deploy: {},
  limits: {
    available: [
      { id: 100, text: '100' },
      { id: 250, text: '250' },
      { id: 500, text: '500' },
      { id: 1000, text: '1000' },
      { id: 0, text: 'Show All' },
    ],
    default: 100,
  },
  filter: {
    skip: 0,
  },
  sort: {
    field: null,
    direction: null,
  },
  meta: {
    count: 0,
    total: 0,
  },
  view: {
    device: {
      item: {},
      issues: [],
    },
    accountEvents: [],
    events: {
      filter: {
        skip: 0,
        limit: 10,
      },
      meta: {
        count: 0,
        total: 0,
      },
      limits: {
        available: [
          { id: 10, text: '10' },
          { id: 100, text: '100' },
          { id: 250, text: '250' },
          { id: 500, text: '500' },
          { id: 1000, text: '1000' },
          { id: 0, text: 'Show All' },
        ],
      },
    },
    addNote: {},
    issues: [],
    saveTags: {},
    tagSuggestions: [],
  },
  list: {
    devices: [],
    issues: {},
  },
};

export default function reducer(state = initialState, action = {}) {
  return ({
    '@@router/LOCATION_CHANGE': () => {
      // ignore any filter changes for devices page
      if (action.payload.pathname.indexOf('/devices') === -1) {
        return state;
      }

      const filter = Object.assign({}, state.filter, action.payload.query || {});

      // use default limit value for first request
      filter.limit = typeof filter.limit !== 'undefined' ? +filter.limit : state.limits.default;

      return {
        ...state,
        filter,
      };
    },
    GET_DEVICES_IN_PROGRESS: () => ({
      ...state,
      inProgress: true,
    }),
    DELETE_DEVICE_IN_PROGRESS: () => ({
      ...state,
      inProgress: true,
    }),
    GET_FIRMWARES_IN_PROGRESS: () => ({
      ...state,
      inProgress: true,
    }),
    DEPLOY_FIRMWARE_IN_PROGRESS: () => ({
      ...state,
      deploy: { inProgress: true, show: true },
      errors: null,
    }),
    GET_DEVICES_COMPLETED: () => ({
      ...state,
      inProgress: false,
      meta: {
        ...state.meta,
        count: action.payload.meta.count,
        total: action.payload.meta.total,
      },
      list: {
        ...state.list,
        devices: ((action.payload || {}).data || []).map(item => item.attributes),
      },
    }),
    GET_PRE_DEVICE: () => ({
      ...state,
      view: {
        ...state.view,
        device: {
          ...state.view.device,
          item: ((state.list.devices || []).filter(d => d.id === action.payload.id) || [])[0],
        },
        events: {
          ...state.view.events,
          hardware: [],
          usage: [],
          meta: {
            ...state.view.events.meta,
            count: 0,
            total: 0,
          },
        },
        accountEvents: [],
      },
    }),
    GET_DEVICE_COMPLETED: () => {
      const issues = (((action.payload || {}).included || []).filter(inc => inc.type === 'issues').map(item => item.attributes) || []);
      const device = (((action.payload || {}).data || []).map(item => item.attributes) || [])[0];

      return {
        ...state,
        inProgress: false,
        view: {
          ...state.view,
          device: {
            ...state.view.device,
            item: device,
            issues,
          },
        },
      };
    },
    GET_DEVICE_IN_PROGRESS: () => ({
      ...state,
      view: {
        ...state.view,
        device: {
          ...state.view.device,
          issues: [],
        },
      },
    }),
    GET_FIRMWARES_COMPLETED: () => ({
      ...state,
      inProgress: false,
      firmwares: ((action.payload || {}).data || []).map(item => item.attributes),
    }),
    DELETE_DEVICE_COMPLETED: () => {
      const updated = _.get(action, 'payload.data[0].attributes');
      return {
        ...state,
        inProgress: false,
        list: {
          ...state.list,
          devices: updated ? _.get(state, 'list.devices', []).map(d => (d.id !== updated.id ? d : updated)) : _.get(state, 'list.devices', []),
        },
      };
    },
    DEPLOY_FIRMWARE_COMPLETED: () => {
      return {
        ...state,
        deploy: { inProgress: false, showResults: true },
        deployResults: (action.payload || {}),
      };
    },
    DEPLOY_FIRMWARE_ERROR: () => {
      return {
        ...state,
        deploy: { inProgress: false, showResults: true },
        deployResults: (action.payload || {}),
      };
    },
    GET_DEVICE_ERROR: () => {
      return {
        ...state,
        errors: _.get(action, 'payload.errors', null),
        inProgress: false,
      };
    },
    SHOW_DEPLOY_FIRMWARE: () => ({
      ...state,
      deploy: { show: true },
    }),
    CANCEL: () => ({
      ...state,
      deploy: {
        ...state.deploy,
        show: false,
      },
      view: {
        ...state.view,
        addNote: {
          ...state.view.addNote,
          show: false,
          errors: [],
          toUpdate: {},
        },
        saveTags: {
          ...state.view.saveTags,
          show: false,
          errors: [],
        },
      },
    }),
    GET_EVENTS_IN_PROGRESS: () => ({
      ...state,
      inProgress: true,
    }),
    GET_EVENTS_ERROR: () => ({
      ...state,
      events: action.payload,
    }),
    GET_EVENTS_COMPLETED: () => {
      const tab = action.payload.tab;
      return {
        ...state,
        inProgress: false,
        view: {
          ...state.view,
          events: {
            ...state.view.events,
            [tab]: {
              data: action.payload.data,
            },
            meta: {
              ...state.view.events.meta,
              count: action.payload.meta.count,
              total: action.payload.meta.total,
            },
          },
        },
      };
    },
    GET_ACCOUNT_EVENTS_COMPLETED: () => ({
      ...state,
      view: {
        ...state.view,
        accountEvents: action.payload,
      },
    }),
    SHOW_ADD_DEVICE_ISSUE: () => ({
      ...state,
      view: {
        ...state.view,
        addNote: {
          ...state.view.addNote,
          show: true,
        },
      },
    }),
    ADD_DEVICE_ISSUE_IN_PROGRESS: () => (_.merge({}, state, {
      view: {
        addNote: {
          show: true,
          inProgress: true,
        },
      },
    })),
    ADD_DEVICE_ISSUE_ERROR: () => (_.merge({}, state, {
      errors: _.get(action, 'payload.errors', null),
      view: {
        addNote: {
          show: true,
          inProgress: false,
        },
      },
    })),
    ADD_DEVICE_ISSUE_COMPLETED: () => {
      const issues = [].concat(_.get(state, 'view.device.issues', [])); // prevent state mutation error
      issues.unshift(_.get(action, 'payload.data[0].attributes'));

      return {
        ...state,
        list: {
          ...state.list,
          issues: {
            ...state.list.issues,
            [action.payload.deviceId]: issues.map(i => ({ attributes: i })),
          },
        },
        view: {
          ...state.view,
          addNote: {
            ...state.view.addNote,
            show: false,
            inProgress: false,
          },
          device: {
            ...state.view.device,
            issues,
          },
        },
      };
    },
    DELETE_DEVICE_ISSUE_IN_PROGRESS: () => ({
      ...state,
    }),
    DELETE_DEVICE_ISSUE_COMPLETED: () => {
      const deletedId = _.get(action, 'payload.data[0].attributes.id');
      const deviceId = _.get(action, 'payload.data[0].attributes.device');
      const issues = [].concat(_.get(state, 'view.device.issues', []).filter(i => i.id !== deletedId));

      return {
        ...state,
        view: {
          ...state.view,
          device: {
            ...state.view.device,
            issues,
          },
        },
        list: {
          ...state.list,
          issues: {
            ...state.list.issues,
            [deviceId]: issues.map(i => ({ attributes: i })),
          },
        },
      };
    },
    NEXT_PAGE: () => {
      switch (action.payload.type) {
        case 'devices':
          return {
            ...state,
            filter: {
              ...state.filter,
              skip: state.filter.skip + state.filter.limit,
            },
          };
        case 'deviceView':
          return {
            ...state,
            view: {
              ...state.view,
              events: {
                ...state.view.events,
                filter: {
                  ...state.view.events.filter,
                  skip: state.view.events.filter.skip + state.view.events.filter.limit,
                },
              },
            },
          };
        default:
          return state;
      }
    },
    PREVIOUS_PAGE: () => {
      switch (action.payload.type) {
        case 'devices':
          return {
            ...state,
            filter: {
              ...state.filter,
              skip: state.filter.skip - state.filter.limit,
            },
          };
        case 'deviceView':
          return {
            ...state,
            view: {
              ...state.view,
              events: {
                ...state.view.events,
                filter: {
                  ...state.view.events.filter,
                  skip: state.view.events.filter.skip - state.view.events.filter.limit,
                },
              },
            },
          };
        default:
          return state;
      }
    },
    SET_PAGE_LIMIT: () => {
      let limit = +action.payload.limit;

      switch (action.payload.type) {
        case 'devices':
          if (_.findIndex(state.limits.available, { id: limit }) === -1) {
            limit = state.limits.default;
          }

          return {
            ...state,
            filter: {
              ...state.filter,
              limit,
              skip: 0, // reset the pagination when the page-size has been changed
            },
          };
        case 'deviceView':
          if (_.findIndex(state.view.events.limits.available, { id: limit }) === -1) {
            limit = state.view.events.limits.default;
          }
          return {
            ...state,
            view: {
              ...state.view,
              events: {
                ...state.view.events,
                filter: {
                  ...state.view.events.filter,
                  limit,
                  skip: 0, // reset the pagination when the page-size has been changed
                },
              },
            },
          };
        default:
          return state;
      }
    },
    RESET_PAGE: () => ({
      ...state,
      filter: {
        ...state.filter,
        skip: 0,
      },
      view: {
        ...state.view,
        events: {
          ...state.view.events,
          filter: {
            ...state.view.events.filter,
            skip: 0,
          },
        },
      },
    }),
    SET_KEYWORDS: () => {
      if (action.payload.path.indexOf('/devices') === -1) return state;
      return {
        ...state,
        filter: {
          ...state.filter,
          keywords: action.payload.keywords,
          skip: 0, // reset the pagination when the page-size has been changed
        },
      };
    },
    DEVICES_SET_SORT_BY: () => {
      const sort = {
        field: action.payload.sortBy,
        direction: state.sort.direction === 'asc' ? 'desc' : 'asc',
      };

      let filterSort = sort.field.split(',').map(f => `${f} ${sort.direction}`).join(',');

      return {
        ...state,
        sort,
        filter: {
          ...state.filter,
          sort: filterSort,
          skip: 0, // reset the pagination when the page-size has been changed
        },
      };
    },
    SHOW_EDIT_DEVICE_ISSUE: () => (_.merge({}, state, {
      view: {
        addNote: {
          errors: [],
          show: true,
          toUpdate: action.payload.issue,
        },
      },
    })),
    EDIT_DEVICE_ISSUE_ERROR: () => (_.merge({}, state, {
      view: {
        addNote: {
          errors: _.get(action, 'payload.errors', null),
          show: true,
          inProgress: false,
        },
      },
    })),
    EDIT_DEVICE_ISSUE_COMPLETED: () => {
      const issue = _.get(action.payload, 'body.data[0].attributes');
      const index = _.findIndex(_.get(state, 'view.device.issues', []), { id: issue.id });
      const issues = [].concat(_.get(state, 'view.device.issues', [])); // prevent mutation error
      issues.splice(index, 1, issue);

      return {
        ...state,
        list: {
          ...state.list,
          issues: {
            ...state.list.issues,
            [action.payload.deviceId]: issues.map(i => ({ attributes: i })),
          },
        },
        view: {
          ...state.view,
          addNote: {
            ...state.view.addNote,
            errors: [],
            show: false,
            toUpdate: null,
            inProgress: false,
          },
          device: {
            ...state.view.device,
            issues,
          },
        },
      };
    },
    EDIT_DEVICE_ISSUE_IN_PROGRESS: () => (_.merge({}, state, {
      view: {
        addNote: {
          show: true,
          inProgress: true,
        },
      },
    })),
    GET_DEVICE_ISSUES_IN_PROGRESS: () => ({
      ...state,
    }),
    GET_DEVICE_ISSUES_COMPLETED: () => {
      return {
        ...state,
        list: {
          ...state.list,
          issues: {
            ...state.list.issues,
            [action.payload.deviceId]: _.get(action, 'payload.data'),
          },
        },
      };
    },
    GET_DEVICE_TAG_SUGGESTIONS_COMPLETED: () => {
      return {
        ...state,
        view: {
          ...state.view,
          tagSuggestions: _.get(action, 'payload.data', []).map(t => t.attributes.value),
        },
      };
    },
    SHOW_ADD_DEVICE_TAGS: () => ({
      ...state,
      view: {
        ...state.view,
        saveTags: {
          ...state.view.saveTags,
          show: true,
        },
      },
    }),
    SAVE_DEVICE_TAGS_IN_PROGRESS: () => ({
      ...state,
      view: {
        ...state.view,
        saveTags: {
          ...state.view.saveTags,
          inProgress: true,
        },
      },
    }),
    SAVE_DEVICE_TAGS_ERROR: () => ({
      ...state,
      view: {
        ...state.view,
        saveTags: {
          ...state.view.saveTags,
          errors: _.get(action, 'payload.errors', null),
          show: true,
          inProgress: false,
        },
      },
    }),
    SAVE_DEVICE_TAGS_COMPLETED: () => {
      const deviceItem = (((action.payload || {}).data || [])
        .map(item => item.attributes) || [])[0];
      return {
        ...state,
        view: {
          ...state.view,
          saveTags: {
            ...state.view.saveTags,
            inProgress: false,
            show: false,
            errors: [],
          },
          tagSuggestions: _.uniq(state.view.tagSuggestions.concat(deviceItem.tags)),
          device: {
            ...state.view.device,
            item: deviceItem,
          },
        },
      };
    },
    EXPORT_DEVICE_LIST_COMPLETED: () => {
      const a = document.createElement('a');
      document.body.appendChild(a);
      a.style = 'display: none';
      const blob = new Blob([(action.payload || {}).data.join('\n')], { type: 'text/csv' });
      const url = window.URL.createObjectURL(blob);
      a.href = url;
      a.download = `devices_${moment().format('YYYY-MM-DD')}.csv`;
      a.click();
      window.URL.revokeObjectURL(url);
    },
  }[action.type] || (() => state))();
}

export const selector = (state) => {
  return state.devices;
};

export const actions = createActions({
  GET_DEVICES: (filter) => {
    const searchParams = {};
    if (typeof filter !== 'undefined') {
      ['keywords'].forEach((value) => {
        if (filter[value]) {
          if (typeof searchParams.filter === 'undefined') searchParams.filter = {};
          searchParams.filter[value] = filter[value];
        }
      });
      ['limit', 'skip', 'sort'].forEach((value) => {
        if (!filter.sort) searchParams.sort = 'activated DESC,lastCheckIn DESC';
        if (typeof filter[value] !== 'undefined') searchParams[value] = filter[value];
      });
    }
    return {
      fetch: true,
      url: `/admin/devices?${queryString.stringify(searchParams)}`,
      method: 'GET',
    };
  },
  GET_PRE_DEVICE: id => ({ id }),
  GET_DEVICE: id => ({
    fetch: true,
    url: `/admin/devices/${id}?include=issues`,
    method: 'GET',
  }),
  GET_FIRMWARES: () => ({
    fetch: true,
    url: '/admin/firmware',
    method: 'GET',
  }),
  DELETE_DEVICE: id => ({
    confirm: 'Are you sure you wish to unlink device?  This will remove the device\'s association with the user.',
    notice: 'Device was unlinked',
    fetch: true,
    url: `/admin/devices/${id}`,
    method: 'DELETE',
  }),
  SHOW_DEPLOY_FIRMWARE: () => {},
  DEPLOY_FIRMWARE: (firmwareId, deviceId, deployTime) => ({
    fetch: true,
    url: '/admin/events/deploy',
    notice: `Device ${deviceId} will be updated with ${firmwareId} firmware`,
    method: 'POST',
    body: { data: [{ type: 'events', attributes: { firmwareId, scheduledAt: deployTime, type: 'firmware.deploy.ids', details: { devices: deviceId } } }] },
  }),
  DEBUG_STATUS: (id, status) => ({
    body: { attributes: { status: { desired: { debug: status } } } },
    fetch: true,
    url: `/admin/devices/${id}`,
    method: 'PUT',
  }),
  CANCEL: () => {},
  GET_EVENTS: (filter, tab) => {
    const searchParams = {};
    if (typeof filter !== 'undefined') {
      ['device', 'type', 'tags'].forEach((value) => {
        if (filter[value]) {
          if (typeof searchParams.filter === 'undefined') searchParams.filter = {};
          searchParams.filter[value] = filter[value];
        }
      });
      ['limit', 'skip', 'sort'].forEach((value) => {
        if (!filter.sort) searchParams.sort = 'createdAt DESC';
        if (typeof filter[value] !== 'undefined') searchParams[value] = filter[value];
      });
    }

    return {
      fetch: true,
      tab,
      url: `/admin/events?${queryString.stringify(searchParams)}`,
      method: 'GET',
    };
  },
  GET_ACCOUNT_EVENTS: (filter) => {
    const searchParams = {};
    if (typeof filter !== 'undefined') {
      ['device', 'type'].forEach((value) => {
        if (filter[value]) {
          if (typeof searchParams.filter === 'undefined') searchParams.filter = {};
          searchParams.filter[value] = filter[value];
        }
      });
      ['limit', 'skip', 'sort'].forEach((value) => {
        if (!filter.sort) searchParams.sort = 'createdAt DESC';
        if (typeof filter[value] !== 'undefined') searchParams[value] = filter[value];
      });
    }

    return {
      fetch: true,
      url: `/admin/events?${queryString.stringify(searchParams)}`,
      method: 'GET',
    };
  },
  ADD_DEVICE_ISSUE: (device, description) => ({
    body: { data: [{ attributes: { device, description } }] },
    fetch: true,
    url: '/admin/issues',
    method: 'POST',
    deviceId: device,
  }),
  SHOW_ADD_DEVICE_ISSUE: () => {},
  DELETE_DEVICE_ISSUE: issueId => ({
    confirm: 'Are you sure you wish to remove note?  This will remove note permanently.',
    notice: 'Note was removed',
    fetch: true,
    url: `/admin/issues/${issueId}`,
    method: 'DELETE',
  }),
  PROVISION_MODE: id => ({
    confirm: 'Are you sure you want to force the device into provision mode? All network settings will be lost.',
    notice: 'Device is now in provision mode',
    fetch: true,
    url: `/admin/devices/${id}`,
    method: 'PUT',
    body: { data: [{ type: 'devices', attributes: { id, status: { desired: { ssid: null } } } }] },
  }),
  NEXT_PAGE: type => ({ type }),
  PREVIOUS_PAGE: type => ({ type }),
  SET_PAGE_LIMIT: (limit, type) => ({ limit, type }),
  RESET_PAGE: () => ({ }),
  SET_KEYWORDS: (path, keywords) => ({ path, keywords }),
  DEVICES_SET_SORT_BY: sortBy => ({ sortBy }),
  SHOW_EDIT_DEVICE_ISSUE: issue => ({ issue }),
  EDIT_DEVICE_ISSUE: attributes => ({
    fetch: true,
    body: { data: [{ type: 'issues', attributes }] },
    url: `/admin/issues/${attributes.id}`,
    method: 'PUT',
    deviceId: attributes.device,
  }),
  GET_DEVICE_ISSUES: (issueId, deviceId) => {
    return {
      fetch: true,
      url: `/admin/issues/${issueId}?sort=updatedAt DESC`,
      method: 'GET',
      deviceId,
    };
  },
  GET_DEVICE_TAG_SUGGESTIONS: () => {
    return {
      fetch: true,
      url: '/admin/device/tags',
      method: 'GET',
    };
  },
  SHOW_ADD_DEVICE_TAGS: () => ({}),
  SAVE_DEVICE_TAGS: (deviceId, tags) => {
    return {
      fetch: true,
      url: `/admin/devices/${deviceId}`,
      method: 'PUT',
      body: { data: [{ type: 'devices', attributes: { tags } }] },
    };
  },
  EXPORT_DEVICE_LIST: (devices) => {
    return {
      fetch: true,
      url: '/admin/devices/export',
      method: 'POST',
      body: { data: devices === 'all' ? [{ type: 'devices', attributes: { id: 'all' } }] : devices.map(d => ({ type: 'devices', attributes: { id: d.id } })) },
    };
  },
});
