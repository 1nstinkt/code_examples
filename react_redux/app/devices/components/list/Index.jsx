/* eslint-disable max-len */

import query from 'qs';
import _ from 'lodash';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import React, { Component } from 'react';
import { Button } from 'react-bootstrap';
import { browserHistory as history } from 'react-router';

import Header from '../../../shared/components/Header';
import DataTable from '../../../shared/components/DataTable';
import Grid from './Grid';

import { selector, actions } from '../../';
import './Styles.scss';

let intervalId = null;

class DevicesList extends Component {
  static search(filter) {
    const searchParams = {};
    if ((filter.keywords || '').trim()) searchParams.keywords = filter.keywords;
    if (typeof filter.limit !== 'undefined') searchParams.limit = +filter.limit;
    if (typeof filter.skip !== 'undefined') searchParams.skip = +filter.skip;

    history.push(`/devices?${query.stringify(searchParams)}`);
  }
  constructor() {
    super();
    this.deleteDevice = this.deleteDevice.bind(this);
    this.deviceChecked = this.deviceChecked.bind(this);
    this.allSelected = this.allSelected.bind(this);
    this.refreshDevice = this.refreshDevice.bind(this);
    this.state = { selected: [], allSelected: false };
  }
  componentDidMount() {
    if (!this.props.list.devices.length) {
      this.props.actions.getDevices(this.props.filter);
    }
    this.props.actions.getDeviceTagSuggestions();
    intervalId = setInterval(() => {
      if (!this.props.inProgress && this.dataTable) this.dataTable.applyKeywords();
    }, 2000);
  }
  componentWillUpdate(next) {
    if (JSON.stringify(this.props.filter) !== JSON.stringify(next.filter)
     || next.refresh
    ) {
      this.props.actions.getDevices(next.filter);
    }
  }
  componentWillUnmount() {
    clearInterval(intervalId);
  }
  deleteDevice(id) {
    this.props.actions.deleteDevice(id);
  }
  deviceChecked(device, value) {
    if (value) {
      this.setState({ selected: _.uniqBy(_.flatten((this.state.selected || []).concat(device)), d => d.id) });
    } else {
      this.setState({
        selected: _.differenceBy(this.state.selected || [], _.flatten([device]), d => d.id),
        allSelected: false,
      });
    }
  }

  allSelected(value) {
    this.setState({ allSelected: !!value });
  }

  refreshDevice() {
    this.props.actions.getDevices(this.props.filter);
  }

  render() {
    return (
      <div>
        <Header />
        <div className="page-container">
          <div className="page-content">
            <div className="content-wrapper">
              <div className="row">
                <div className="col-lg-12 panel panel-flat">
                  <div className="panel-heading">
                    <h4>All Knocki Devices</h4>
                    <div className="heading-elements">
                      <ul className="icons-list">
                        <li>
                          <a data-action="collapse" >&nbsp;</a>
                        </li>
                        <li>
                          <Button onClick={this.refreshDevice} className="glyphicon glyphicon-refresh" id="refresh" />
                        </li>
                      </ul>
                    </div>
                  </div>
                  <div className="panel-body">
                    <div className="col-lg-12">
                      <div className="tab-content">
                        <div className="tab-pane active">
                          <DataTable
                            ref={(ref) => { this.dataTable = ref; }}
                            type="devices"
                            onChange={DevicesList.search}
                            suggestions={this.props.view.tagSuggestions}
                            selected={this.state.selected}
                            allSelected={this.state.allSelected}
                            onExport={(devices) => { this.props.actions.exportDeviceList(devices); }}
                            {...this.props}
                          >
                            <Grid
                              devices={this.props.list.devices}
                              inProgress={this.props.inProgress}
                              deleteDevice={this.deleteDevice}
                              onDeviceChecked={this.deviceChecked}
                              onAllSelected={this.allSelected}
                              selected={this.state.selected}
                              {...this.props}
                            />
                          </DataTable>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

DevicesList.propTypes = {
  actions: React.PropTypes.shape({
    getDevices: React.PropTypes.func,
    deleteDevice: React.PropTypes.func,
  }).isRequired,
  filter: React.PropTypes.shape({}),
  list: React.PropTypes.shape({
    devices: React.PropTypes.arrayOf(React.PropTypes.shape()),
  }),
  inProgress: React.PropTypes.bool,
};

DevicesList.defaultProps = {
  filter: {},
  list: {
    devices: [],
  },
  inProgress: false,
};

export default connect(selector, dispatch => ({
  actions: bindActionCreators(actions, dispatch),
}))(DevicesList);
